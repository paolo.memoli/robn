const Influx = require('influx');

module.exports = {
  influxdb: {
		host: 'influxdb.100shapes.com',
    port: 5000,
		database: 'robn-db',
    username: process.env.INFLUXDB_USER,
    password: process.env.INFLUXDB_PASSWORD,
		schema: [
		 {
		   measurement: 'risks',
		   fields: {
		     message: Influx.FieldType.STRING,
         risk: Influx.FieldType.STRING,
         profanitySeverity: Influx.FieldType.FLOAT,
		   },
		   tags: [
		      'source',
          'user',
          'company',
          'department',
          'team',
          'source_team',
          'channel',
          'ruleId',
		   ]
		 },
    ]
	}
};
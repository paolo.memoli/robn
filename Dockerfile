FROM node:8-alpine

RUN apk add --no-cache make gcc g++ python

ADD package.json /var/app/

WORKDIR /var/app

RUN yarn
ADD . /var/app/

FROM node:8-alpine
WORKDIR /var/app

ENV PORT 80

ENV TOKEN 'Change me'

ENV INFLUXDB_USER 'Change me'
ENV INFLUXDB_PASSWORD 'Change me'

COPY --from=0 /var/app/ .
VOLUME /opt/local/

EXPOSE 80

CMD ["./node_modules/pm2/bin/pm2-docker", "index.js"]
